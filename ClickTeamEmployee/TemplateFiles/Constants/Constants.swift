//
//  Constants.swift
//  The Court Lawyer
//
//  Created by Ahmed Shahid on 5/3/18.
//  Copyright © 2018 Ahmed Shahid. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

struct Global{
    
    static var APP_MANAGER                   = AppStateManager.sharedInstance
    static var APP_REALM                     = APP_MANAGER.realm
    static var APP_COLOR_RED                     = UIColor(red:0.80, green:0.25, blue:0.25, alpha:1.0)
    static var APP_COLOR                         = UIColor(hexString: "00679B")
    //    static var USER                          = APP_MANAGER.loggedInUser
}

struct Constants {
    
    static let APP_DELEGATE                = UIApplication.shared.delegate as! AppDelegate
    static let UIWINDOW                    = UIApplication.shared.delegate!.window!
    
    static let USER_DEFAULTS               = UserDefaults.standard
    
    //static let SINGLETON                   = Singleton.sharedInstance
    
    static let DEFAULTS_USER_KEY           = "User"
    
    //MARK: - THEME COLORS
    static let THEME_ORANGE_COLOR          = UIColor(red: 0xFF, green: 0x7E, blue: 0x5B) //FF-7E-5B
    static let FIELD_VALIDATION_RED_COLOR       = UIColor(red: 0xC4, green: 0x13, blue: 0x02) //C41302
    static var DeviceToken                     = "123456789"
    //MARK: - Base URLs
    static let BaseURL                     = "http://arbaeenapp.uhfsolutions.com/mobileservice/mobileservice.php?action="
    
    static let serverDateFormat = "yyyy-MM-dd HH:mm:ss"
    static let PAGINATION_PAGE_SIZE        = 100
    
    static var api_access_key = ""
}

