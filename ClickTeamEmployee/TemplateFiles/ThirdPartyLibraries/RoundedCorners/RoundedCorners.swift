//
//  RoundedCorners.swift
//  Fan Direct
//
//  Created by Usman Bin Rehan on 2/26/18.
//  Copyright © 2018 Usman Bin Rehan. All rights reserved.
//

import UIKit

class RoundedButton: UIButton {
    override func layoutSubviews() {
        super.layoutSubviews()
        let height = self.frame.height
        self.layer.cornerRadius = CGFloat(height / 2.0)
        self.clipsToBounds = true
        
    }
}
class RoundedLabel: UILabel {
    override func layoutSubviews() {
        super.layoutSubviews()
        let height = self.frame.height
        self.layer.cornerRadius = CGFloat(height / 2.0)
        self.clipsToBounds = true
        
    }
}
class RoundedImage : UIImageView{
    override func layoutSubviews() {
        super.layoutSubviews()
        let height = self.frame.height
        self.layer.cornerRadius = CGFloat(height / 2.0)
        self.clipsToBounds = true
    }
}
class RoundedView : UIView{
    override func layoutSubviews() {
        super.layoutSubviews()
        let height = self.frame.height
        self.layer.cornerRadius = CGFloat(height / 2.0)
        self.clipsToBounds = true
    }
}
class RoundedSwitch : UISwitch{
    override func layoutSubviews() {
        super.layoutSubviews()
        let height = self.frame.height
        self.layer.cornerRadius = CGFloat(height / 2.0)
        self.clipsToBounds = true
    }
}
