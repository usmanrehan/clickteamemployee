//
//  UserAPIManager.swift
//  The Court Lawyer
//
//  Created by Ahmed Shahid on 5/3/18.
//  Copyright © 2018 Ahmed Shahid. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

class UsersAPIManager: APIManagerBase {
    //MARK:- LOG IN
    func login(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route : URL = POSTURLforRoute(route: Constants.BaseURL)!
        print(route)
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    
    
    //MARK:- Get Array Response
    func getArrayResponse(params:Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route : URL = URLforRoute(route: Constants.BaseURL, params: params)! as URL
        print(route)
        self.getRequestForArrayWith(route: route, success: success, failure: failure, withHeader: false)
    }
    //MARK:- Get Dictionary Response
    func getDictionaryResponse(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route : URL = URLforRoute(route: Constants.BaseURL, params: params)! as URL
        print(route)
        self.getRequestForDictionaryWith(route: route, success: success, failure: failure, withHeader: false)
    }
    //MARK:- Get Array Response
    func postArrayResponse(params:Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
//        let route : URL = NSURL(string:"http://arbaeenapp.uhfsolutions.com/mobileservice/mobileservice.php")! as URL
        let route : URL = URLforRoute(route: Constants.BaseURL, params: params)! as URL
        print(route)
        self.postRequestArrayWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
}
