//
//  Users.swift
//
//  Created by Hamza Hasan on 19/11/2018
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class User: Object, Mappable {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let creationDate = "creation_date"
        static let userType = "user_type"
        static let dob = "dob"
        static let groupLead = "group_lead"
        static let profileImg = "profile_img"
        static let gender = "gender"
        static let phoneNumber = "phone_number"
        static let designation = "designation"
        static let androidPushId = "android_push_id"
        static let status = "status"
        static let lastName = "last_name"
        static let apiAccessToken = "api_access_token"
        static let firstName = "first_name"
        static let comments = "comments"
        static let applePushId = "apple_push_id"
        static let emailAddress = "email_address"
        static let pinIcon = "pin_icon"
        static let empCode = "emp_code"
        static let loginId = "login_id"
        static let userId = "user_id"
        static let apiAccessKey = "apiAccessKey"
    }
    
    // MARK: Properties
    @objc dynamic var creationDate: String? = ""
    @objc dynamic var userType: String? = ""
    @objc dynamic var dob: String? = ""
    @objc dynamic var groupLead: String? = ""
    @objc dynamic var profileImg: String? = ""
    @objc dynamic var gender: String? = ""
    @objc dynamic var phoneNumber: String? = ""
    @objc dynamic var designation: String? = ""
    @objc dynamic var androidPushId: String? = ""
    @objc dynamic var status: String? = ""
    @objc dynamic var lastName: String? = ""
    @objc dynamic var apiAccessToken: String? = ""
    @objc dynamic var firstName: String? = ""
    @objc dynamic var comments: String? = ""
    @objc dynamic var applePushId: String? = ""
    @objc dynamic var emailAddress: String? = ""
    @objc dynamic var pinIcon: String? = ""
    @objc dynamic var empCode: String? = ""
    @objc dynamic var loginId: String? = ""
    @objc dynamic var userId: String? = ""
    @objc dynamic var apiAccessKey: String? = ""
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    required convenience public init?(map : Map){
        self.init()
    }
    
    override public class func primaryKey() -> String? {
        return "userId"
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        creationDate <- map[SerializationKeys.creationDate]
        userType <- map[SerializationKeys.userType]
        dob <- map[SerializationKeys.dob]
        groupLead <- map[SerializationKeys.groupLead]
        profileImg <- map[SerializationKeys.profileImg]
        gender <- map[SerializationKeys.gender]
        phoneNumber <- map[SerializationKeys.phoneNumber]
        designation <- map[SerializationKeys.designation]
        androidPushId <- map[SerializationKeys.androidPushId]
        status <- map[SerializationKeys.status]
        lastName <- map[SerializationKeys.lastName]
        apiAccessToken <- map[SerializationKeys.apiAccessToken]
        firstName <- map[SerializationKeys.firstName]
        comments <- map[SerializationKeys.comments]
        applePushId <- map[SerializationKeys.applePushId]
        emailAddress <- map[SerializationKeys.emailAddress]
        pinIcon <- map[SerializationKeys.pinIcon]
        empCode <- map[SerializationKeys.empCode]
        loginId <- map[SerializationKeys.loginId]
        userId <- map[SerializationKeys.userId]
        apiAccessKey <- map[SerializationKeys.apiAccessKey]
    }
    
    
}
