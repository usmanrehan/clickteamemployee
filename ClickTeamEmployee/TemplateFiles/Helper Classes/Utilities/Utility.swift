import Foundation
import UIKit
import AVFoundation
import NVActivityIndicatorView

//// MARK: AppHelperUtility setup
@objc class Utility: NSObject
{
    static let main = Utility()
    fileprivate override init() {}
}


extension Utility {
    
    func roundAndFormatFloat(floatToReturn : Float, numDecimalPlaces: Int) -> String{
        
        let formattedNumber = String(format: "%.\(numDecimalPlaces)f", floatToReturn)
        return formattedNumber
        
    }
    func printFonts() {
        for familyName in UIFont.familyNames {
            print("\n-- \(familyName) \n")
            for fontName in UIFont.fontNames(forFamilyName: familyName) {
                print(fontName)
            }
        }
    }
    
    func topViewController(base: UIViewController? = (Constants.APP_DELEGATE).window?.rootViewController) -> UIViewController? {
        
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
    
    
    //     func showAlert(title:String?, message:String?) {
    //        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    //        alert.addAction(UIAlertAction(title: "OK", style: .default) { _ in })
    //        Utility().topViewController()!.present(alert, animated: true){}
    //    }
    
    func resizeImage(image: UIImage,  targetSize: CGFloat) -> UIImage {
        
        guard (image.size.width > 1024 || image.size.height > 1024) else {
            return image;
        }
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newRect: CGRect = CGRect.zero;
        
        if(image.size.width > image.size.height) {
            newRect.size = CGSize(width: targetSize, height: targetSize * (image.size.height / image.size.width))
        } else {
            newRect.size = CGSize(width: targetSize * (image.size.width / image.size.height), height: targetSize)
        }
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newRect.size, false, 1.0)
        image.draw(in: newRect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    
    func thumbnailForVideoAtURL(url: URL) -> UIImage? {
        
        let asset = AVAsset(url: url)
        let assetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImageGenerator.appliesPreferredTrackTransform=true
        
        var time = asset.duration
        time.value = min(time.value, 2)
        
        do {
            let imageRef = try assetImageGenerator.copyCGImage(at: time, actualTime: nil)
            return UIImage(cgImage: imageRef)
        } catch {
            print("error")
            return nil
        }
    }
    
    
    func delay(delay:Double, closure:@escaping ()->()) {
        DispatchQueue.main.asyncAfter(
            deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
    }
    
    func showLabelIfNoData(withtext text: String, controller: UIViewController, tableView: UITableView)
    {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: controller.view.bounds.size.width, height: controller.view.bounds.size.height))
        messageLabel.text = text//"No voucher is currently available. Please pull down to refresh."
        messageLabel.textColor = UIColor.black
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont(name: "Palatino-Italic", size: 20) ?? UIFont()
        messageLabel.sizeToFit()
        
        tableView.backgroundView = messageLabel;
        tableView.separatorStyle = UITableViewCellSeparatorStyle.none;
    }
    
    static func showAlert(errorMessage: String){
        Utility.hideLoader()
        self.main.showAlert(message: errorMessage, title: "Message")
        
//        let alertView = AlertView.instanceFromNib() as! AlertView
//        alertView.lblError.text = errorMessage
//        alertView.showAnimate()
//        let window = UIApplication.shared.keyWindow!
//        alertView.frame = (window.frame)
//        window.addSubview(alertView)
//        Utility.hideLoader()
    }
}
// MARK:- INDICATOR
extension Utility
{
    static func showLoader() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let size = CGSize(width: 50, height: 50)
        let bgColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        let activityData = ActivityData(size: size, message: "", messageFont: UIFont.systemFont(ofSize: 12), type: .ballClipRotate, color: Global.APP_COLOR, padding: 0, displayTimeThreshold: 0, minimumDisplayTime: 1, backgroundColor: bgColor, textColor: UIColor.black)
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, nil)
    }
    
    static func hideLoader() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
    }
}

// MARK: Alert related functions
extension Utility
{
    func showAlert(message:String,title:String,controller:UIViewController) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
//        alertController.view.tintColor = Constants.THEME_COLOR_GOLDEN
        alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        controller.present(alertController, animated: true, completion: nil)
    }
    
    func showAlertWithOptionalController(message:String,title:String,controller:UIViewController?) -> UIViewController?{
        if(controller == nil)
        {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            
            return alertController
        }
        else
        {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            controller!.present(alertController, animated: true, completion: nil)
            
            return nil
        }
    }
    
    func showAlert(message: String, title: String, controller: UIViewController, usingCompletionHandler handler:@escaping (() -> Swift.Void))
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {
            
            (action) in
            
            handler()
        }
        ))
        controller.present(alertController, animated: true, completion: nil)
    }
    
    func showAlertPermission(message:String,title:String,controller:UIViewController, completionHandler: @escaping (UIAlertAction?, UIAlertAction?) -> Void){
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
//        alertController.view.tintColor = Constants.THEME_COLOR_GOLDEN
        alertController.addAction(UIAlertAction(title: "YES", style: UIAlertActionStyle.default){ (alertActionYES) in
            completionHandler(alertActionYES, nil)
        })
        
        alertController.addAction(UIAlertAction(title: "NO", style: UIAlertActionStyle.cancel){ (alertActionNO) in
            completionHandler(nil, alertActionNO)
        })
        
        controller.present(alertController, animated: true, completion: nil)
        
    }
    
    func showAlert(message:String,title:String){
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            
            let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            topController.present(alertController, animated: true, completion: nil)
            
        }
    }
    
    func convertToArray(text: String) -> Array<Any>? {
        if let data = text.data(using: String.Encoding.utf8) {
            NSLog("Enter here")
            do {
                return try JSONSerialization.jsonObject(with: data, options: [.allowFragments, .mutableContainers, .mutableLeaves]) as? Array
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
        
    }
    
    func showActivityViewController(with sharingText: String)
    {
        let activityViewController = UIActivityViewController(
            activityItems: [sharingText],
            applicationActivities: nil)
        let topController = UIApplication.shared.keyWindow?.rootViewController
        topController?.present(activityViewController, animated: true, completion: nil)
    }
    
}

// MARK:- INDICATOR
extension Utility
{
    func showLoader() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let size = CGSize(width: 50, height: 50)
        let bgColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        let activityData = ActivityData(size: size, message: "", messageFont: UIFont.systemFont(ofSize: 12), type: .ballClipRotate, color: .white , padding: 0, displayTimeThreshold: 0, minimumDisplayTime: 1, backgroundColor: bgColor, textColor: UIColor.black)
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, nil)
    }
    
    func hideLoader() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
    }
}
// MARK:- TOAST HELPER UTILITY
extension Utility
{
    func showToast(message: String, controller: UIViewController)
    {
        controller.view.hideToastActivity()
        controller.view.makeToast(message, duration: 3.0, position: .center)
    }

    func showToast(message: String, controller: UIViewController, position: ToastPosition)
    {
        controller.view.hideToastActivity()
        controller.view.makeToast(message, duration: 3.0, position: position)
    }
    func showToast(message: String, controller: UIViewController, duration: TimeInterval)
    {
        controller.view.hideToastActivity()
        controller.view.makeToast(message, duration: duration, position: .center)

    }

    func showToast(message: String)
    {
        let topController = UIApplication.shared.keyWindow?.rootViewController
        topController?.view.hideToastActivity()
        topController?.view.makeToast(message, duration: 3.0, position: .center)
    }
    func dialNumber(number : String) {
        
        if let url = URL(string: "tel://\(number)"),
            UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler:nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        } else {
            // add error message here
        }
    }
}
// MARK:- Utility Methods
extension Utility{
    static func stringDateFormatter(dateStr: String , dateFormat : String , formatteddate : String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.current
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = dateFormat
        let date = dateFormatter.date(from: dateStr)
        dateFormatter.dateFormat = formatteddate
        dateFormatter.timeZone = TimeZone.current
        if date == nil{
            return "In valid format"
        }
        return dateFormatter.string(from: date!)
    }
    static func stringCustomCurrentDate(requiredFormat : String) -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = requiredFormat
        
        let currentDateString = dateFormatterGet.string(from: Date())
        
        if let date = dateFormatterGet.date(from: currentDateString){
            return dateFormatterPrint.string(from: date)
        }
        else {
            return "23 May, 2018"
        }
    }
    static func stringCustomCurrentDatePre(value:Int ,requiredFormat : String) -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = requiredFormat
        
        let currentDateString = dateFormatterGet.string(from: Calendar.current.date(byAdding: .day, value: value, to: Date())!)
        
        if let date = dateFormatterGet.date(from: currentDateString){
            return dateFormatterPrint.string(from: date)
        }
        else {
            return "23 May, 2018"
        }
    }
    static func showLanguageAlert(message:String,title:String,controller:UIViewController, completionHandler: @escaping (UIAlertAction?, UIAlertAction?) -> Void){
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alertController.view.tintColor = .black
        alertController.addAction(UIAlertAction(title: "YES", style: UIAlertActionStyle.default){ (alertActionYES) in
            completionHandler(alertActionYES, nil)
        })
        alertController.addAction(UIAlertAction(title: "NO", style: UIAlertActionStyle.cancel){ (alertActionNO) in
            completionHandler(nil, alertActionNO)
        })
        controller.present(alertController, animated: true, completion: nil)
    }
    static func stringToUrl(string: String)-> URL{
        if Validation.verifyUrl(urlString: string){
            return URL(string: string.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)!
        }
        else{
            let placeHolder = "http://www.google.com"
            return URL(string: placeHolder.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)!
        }
    }
}
